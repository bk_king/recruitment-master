<?php
/**
 * Author: Bartosz Król
 * Date: 14.02.2018
 * Time: 21:25
 */
declare(strict_types=1);

namespace Gwo\Recruitment\Cart;

use Gwo\Recruitment\Cart\Exception\QuantityTooLowException;
use Gwo\Recruitment\Entity\Product;

class Item implements ItemInterface
{
    const INVALID_MINIMUM_QUANTITY = 'Quantity is to low';
    private $product;
    private $quantity;

    public function __construct(Product $product, int $quantity)
    {
        $this->product = $product;
        $this->setQuantity($quantity);
    }

    /**
     * @param int $quantity
     * @throws QuantityTooLowException
     * @return ItemInterface
     */
    public function setQuantity(int $quantity): ItemInterface
    {
        if ($quantity < $this->product->getMinimumQuantity()) {
            throw new QuantityTooLowException(self::INVALID_MINIMUM_QUANTITY);
        }
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalPrice(): int
    {
        $price = $this->product->getUnitPrice();
        return $price * $this->quantity;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }
}