<?php
/**
 * Author: Bartosz Król
 * Date: 16.02.2018
 * Time: 20:57
 */
declare(strict_types=1);

namespace Gwo\Recruitment\Cart;

interface ItemInterface
{
    /**
     * @param int $quantity
     * @return $this
     */
    public function setQuantity(int $quantity): ItemInterface;
}