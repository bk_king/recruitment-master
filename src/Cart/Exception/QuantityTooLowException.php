<?php
/**
 * Author: Bartosz Król
 * Date: 15.02.2018
 * Time: 21:37
 */

namespace Gwo\Recruitment\Cart\Exception;

class QuantityTooLowException extends \Exception
{

}