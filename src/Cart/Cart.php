<?php
/**
 * Author: Bartosz Król
 * Date: 14.02.2018
 * Time: 21:25
 */
declare(strict_types=1);

namespace Gwo\Recruitment\Cart;

use Gwo\Recruitment\Entity\Product;

class Cart
{
    const INVALID_QUANTITY = 'Argument less then %d';
    private $items = [];

    /**
     * @param Product $product
     * @param $quantity
     * @return $this
     */
    public function addProduct(Product $product, int $quantity): Cart
    {
        $item = $this->isItemExists($product);
        if ($item !== null) {
            $existingQuantity = $item->getQuantity();
            $item->setQuantity($existingQuantity + $quantity);
        } else {
            $this->items[] = new Item($product, $quantity);
        }
        return $this;
    }

    /**
     * @param Product $product
     * @return Item|null
     */
    private function isItemExists(Product $product): ?Item
    {
        foreach ($this->items as $item) {
            if ($item->getProduct() === $product) {
                return $item;
            }
        }
        return null;
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param $index
     * @return Item
     * @throws \OutOfBoundsException
     */
    public function getItem($index): Item
    {
        if (false === array_key_exists($index, $this->items)) {
            throw new \OutOfBoundsException();
        }
        return $this->items[$index];
    }

    /**
     * @return int
     */
    public function getTotalPrice(): int
    {
        $price = 0;
        foreach ($this->items as $item) {
            $price += $item->getTotalPrice();
        }
        return $price;
    }

    /**
     * @param Product $product
     * @param $quantity
     * @return $this
     */
    public function setQuantity(Product $product, int $quantity): Cart
    {
        foreach ($this->items as $item) {
            if ($item->getProduct() === $product) {
                $item->setQuantity($quantity);
                break;
            }
        }
        return $this;
    }

    /**
     * @param Product $product
     * @return bool
     */
    public function removeProduct(Product $product): bool
    {
        $result = [];
        foreach ($this->items as $item) {
            if ($item->getProduct() !== $product) {
                $result[] = $item;
            }
        }
        $this->items = $result;
        return count($result) < count($this->items);
    }

}