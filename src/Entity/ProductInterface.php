<?php
/**
 * Author: Bartosz Król
 * Date: 15.02.2018
 * Time: 21:28
 */

namespace Gwo\Recruitment\Entity;

interface ProductInterface
{
    /**
     * @return int
     */
    public function getUnitPrice(): int;

    /**
     * @return int
     */
    public function getMinimumQuantity(): int;
}