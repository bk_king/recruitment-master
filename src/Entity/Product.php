<?php
/**
 * Author: Bartosz Król
 * Date: 14.02.2018
 * Time: 21:25
 */
declare(strict_types=1);

namespace Gwo\Recruitment\Entity;

class Product implements ProductInterface
{
    const INVALID_ARGUMENT = 'Argument less then 1!';
    private $id;
    private $unitPrice;
    private $minimumQuantity;

    public function __construct()
    {
        $this->minimumQuantity = 1;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setId($value): Product
    {
        $this->id = $value;
        return $this;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setUnitPrice(int $value): Product
    {
        if ($value < 1) {
            throw new \InvalidArgumentException(self::INVALID_ARGUMENT);
        }
        $this->unitPrice = $value;
        return $this;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setMinimumQuantity(int $value): Product
    {
        if ($value < 1) {
            throw new \InvalidArgumentException(self::INVALID_ARGUMENT);
        }
        $this->minimumQuantity = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getUnitPrice(): int
    {
        return $this->unitPrice;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getMinimumQuantity(): int
    {

        return $this->minimumQuantity;
    }

}